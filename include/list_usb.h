#include <libudev.h>
#include <stdlib.h>
#include <stdio.h>
#include <jWrite.h>


#define MAX_BUFFER      102400

struct udev_device* get_child(struct udev* udev, struct udev_device* parent, const char* subsystem);

 char * enumerate_usb_mass_storage(struct udev* udev);
