#
# Example program
#
#
CC=gcc
CFLAGS=-c -Wall -D_REENTRANT $(ADDITIONALFLAGS) -I$(ULFIUS_LOCATION)
ULFIUS_LOCATION=./ulfius/src
LIBS=-lc -ljansson -lyder -lorcania -lulfius -L$(ULFIUS_LOCATION)
INCLUDE=-I./include
SRC=./lib/list_usb.c
LIB=./lib/libjwrite.a


all: client server

clean:
	rm -f *.o client server mail

debug: ADDITIONALFLAGS=-DDEBUG -g -O0

debug: client server mail

libulfius.so:
	cd $(ULFIUS_LOCATION) && $(MAKE) debug WEBSOCKETFLAG=-DU_DISABLE_WEBSOCKET

client.o: client.c libulfius.so
	$(CC) $(CFLAGS) client.c -DDEBUG -g -O0

client: client.o
	$(CC) -o client client.o $(LIBS)

server.o: server.c libulfius.so
	$(CC) $(CFLAGS) server.c -DDEBUG -g -O0 $(INCLUDE)

server: server.o
	$(CC) -o server server.o $(LIBS) $(SRC) $(INCLUDE) $(LIB) -ludev

