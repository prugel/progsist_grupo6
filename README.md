
Proyecto Final 2017 - I
Programación de Sistemas
Autores: Pablo Rugel prugel@espol.edu.ec
 	 Jorge Encalada jdencala@espol.edu.ec

Monitoreo y control de dispositivos USB mediante un API REST.

La aplicación deberá funcionar como un daemon de Linux y consistir en dos procesos: MonitorUSB y Web Server. Ambos procesos se comunican entre sí usando una interface de sockets. El proceso Monitor USB debe de monitorear y llevar un registro de los dispositivos USB de almacenamiento masivo conectados al computador mientras que el proceso Web Server debe de implementar un API REST que permita a usuarios remotos conocer e interactuar con los dispositivos USB conectados.


Compilador GCC para Ubuntu

//Instalacion de librerias
$ sudo apt-get install libusb-1.0-0-dev
$ sudo apt-get install libudev-dev

//Libreria ulfius.h
$ sudo apt-get install libmicrohttpd-dev libjansson-dev libcurl4-gnutls-dev libgnutls28-dev libgcrypt20-dev

//Listar dispositivos de almacenamiento USB usando libudev. 
gcc -o list list_usb.c -ludev
./list



Manual de instalacion:
Instalacion:

$ git clone https://github.com/babelouest/ulfius.git
$ cd ulfius/
$ git submodule update --init
$ cd lib/orcania
$ make && sudo make install
$ cd ../yder
$ make && sudo make install
$ cd ../..
$ make
$ sudo make install

Actualizar Ulfius
$ cd ulfius/
$ git pull 
$ git submodule update
$ cd lib/orcania
$ make && sudo make install
$ cd ../yder
$ make && sudo make install
$ cd ../..
$ make
$ sudo make install


Enlace del repositorio en bitbucket:

git clone https://prugel@bitbucket.org/prugel/progsist_grupo6.git

$ cd progsist_grupo6

$ make

Abrir 2 terminales:
1.- Ejecutar ./client

Donde se veran reflejados los dispositivos de almacenamiento USB

2.- Ejecutar ./server

Se levanta el servidor web

3.- Abrir un navegador web e introducir la direccion: http://localhost:7778/dispositivos

4.- Se visualiza la informacion desplegada en el navegador web en formato JSON

Si se desconecta un dispositivo o se conecta uno nuevo, se actualiza la informacion desplegada en el navegador






