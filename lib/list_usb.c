
//Instalar librerias
//sudo apt-get install libusb-1.0-0-dev
//sudo apt-get install libudev-dev



/* List USB storage devices using libudev.
 *
 * gcc -o udev_list_usb_storage udev_list_usb_storage.c -ludev
 * ./udev_list_usb_storage



////PARA CORRER EL USB/////
g++ -o list list_usb.cpp -ludev
./list


sudo apt install cmake


////libreria  ulfius.h////
apt-get install libmicrohttpd-dev libjansson-dev libcurl4-gnutls-dev libgnutls28-dev libgcrypt20-dev


///Manual install  ///  libreria  ulfius.h

# apt-get install libmicrohttpd-dev libjansson-dev libcurl4-gnutls-dev libgnutls28-dev libgcrypt20-dev

Installation


$ git clone https://github.com/babelouest/ulfius.git
$ cd ulfius/
$ git submodule update --init
$ cd lib/orcania
$ make && sudo make install
$ cd ../yder
$ make && sudo make install
$ cd ../..
$ make
$ sudo make install

//Update Ulfius
$ cd ulfius/
$ git pull # Or git checkout <the version you need>
$ git submodule update
$ cd lib/orcania
$ make && sudo make install
$ cd ../yder
$ make && sudo make install
$ cd ../..
$ make
$ sudo make install


jorge encalada: jdencalahuaya@gmail.com

 */
#include <list_usb.h>
#include <string.h>

struct udev_device* get_child(struct udev* udev, struct udev_device* parent, const char* subsystem)
{
    struct udev_device* child = NULL;
    struct udev_enumerate *enumerate = udev_enumerate_new(udev);

    udev_enumerate_add_match_parent(enumerate, parent);
    udev_enumerate_add_match_subsystem(enumerate, subsystem);
    udev_enumerate_scan_devices(enumerate);

    struct udev_list_entry *devices = udev_enumerate_get_list_entry(enumerate);
    struct udev_list_entry *entry;

    udev_list_entry_foreach(entry, devices) {
        const char *path = udev_list_entry_get_name(entry);
        child = udev_device_new_from_syspath(udev, path);
        break;
    }

    udev_enumerate_unref(enumerate);
    return child;
}

char * enumerate_usb_mass_storage(struct udev* udev)
{

	char *buffer = (char * )malloc(MAX_BUFFER);
	unsigned int buflen= MAX_BUFFER;

    	struct udev_enumerate* enumerate = udev_enumerate_new(udev);

    	udev_enumerate_add_match_subsystem(enumerate, "scsi");
    	udev_enumerate_add_match_property(enumerate, "DEVTYPE", "scsi_device");
    	udev_enumerate_scan_devices(enumerate);

    	struct udev_list_entry *devices = udev_enumerate_get_list_entry(enumerate);
    	struct udev_list_entry *entry;
	
	jwOpen(buffer, buflen, JW_OBJECT, JW_PRETTY);	
	
	jwObj_array("Dispositivos");

    	udev_list_entry_foreach(entry, devices) {

        const char* path = udev_list_entry_get_name(entry);
        struct udev_device* scsi = udev_device_new_from_syspath(udev, path);

        struct udev_device* block = get_child(udev, scsi, "block");
        struct udev_device* scsi_disk = get_child(udev, scsi, "scsi_disk");
	

        struct udev_device* usb
            = udev_device_get_parent_with_subsystem_devtype(scsi, "usb", "usb_device");

        if (block && scsi_disk && usb) {


		jwArr_object();

		jwObj_string("Nodo", strdup(udev_device_get_devnode(block)));
		jwObj_string("Vendor", strdup(udev_device_get_sysattr_value(scsi, "vendor")));
		jwObj_string("idVendor",strdup(udev_device_get_sysattr_value(usb, "idVendor")));
		jwObj_string("idProduct", strdup(udev_device_get_sysattr_value(usb, "idProduct")));

		jwEnd();

        
        }
        if (block) {
            udev_device_unref(block);
        }

        if (scsi_disk) {
            udev_device_unref(scsi_disk);
        }

        udev_device_unref(scsi);
    }
	
	jwEnd();
	jwClose();

    udev_enumerate_unref(enumerate);

return buffer;
}

// int main()
// {
	
   
//     struct udev* udev = udev_new();

//     enumerate_usb_mass_storage(udev);

//     udev_unref(udev);
//     return 0;
// }
